import com.zuitt.example.Car;
import com.zuitt.example.Dog;

public class Main {
    public static void main(String[] args) {

//        System.out.println("Hello world!");
        Car myCar = new Car();
        myCar.drive();
        System.out.println(myCar.getDriverName());
        // setters
        myCar.setBrand("Kia");
        myCar.setName("Sorento");
        myCar.setYear_make(2022);
        // getters
        System.out.println("The " + myCar.getBrand() + " " + myCar.getName() + " " + myCar.getYear_make() + " was driven by " + myCar.getDriverName());

        myCar.setBrand("Toyota");
        System.out.println("My new car is " + myCar.getBrand());

        // another Car
        Car yourCar = new Car();
        System.out.println("Your car was driven by: " + yourCar.getDriverName());

        // Animal and Dog
        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        System.out.println(myPet.getName() + " " + myPet.getColor() + " " + myPet.getBreed());

        Dog yourPet = new Dog();
        System.out.println(yourPet.getBreed() + " " + yourPet.getName());
    }
}